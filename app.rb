require 'oauth'
require 'oauth/server'
require 'oauth/signature'
require 'oauth/request_proxy/rack_request'
require 'net/http'
require 'uri'
require 'cgi'

# Generates remote app registration XML
get "/register" do
  content_type :xml
  erb :registration, :locals => { 
    :appkey => ENV['APP_KEY'],
    :baseurl => request.url.split('/',5)[0,3].join('/'),
    :publicKey => ENV['LOCAL_PUBLIC_KEY']
  }
end

# Filter to reject unauthorized requests against the macro
before "/macro" do
  halt(402, "Not authorized") unless env['HTTP_AUTHORIZATION']
  valid = OAuth::Signature.verify(request) do |token|
    [ "", ENV['HOST_PUBLIC_KEY'] ]
  end
  halt(401, "Not authorized") unless valid
end

get "/macro" do
  # Send contents of macro body to Web Sequence Diagrams to generate image
  format = params['format'] || "png"
  style = params['style'] || "default"
  response = Net::HTTP.post_form(URI.parse("http://www.websequencediagrams.com/index.php"),{
    :style => style,
    :format => format,
    :message => params['body'],
    :apiVersion => 1
  })
  out = MultiJson.decode(response.body)

  # Extract image then upload to Confluence. The Web Sequence Diagrams service deletes
  # the generated images after a short time. It's best to store the generated images
  # inside the Confluence page.
  response = Net::HTTP.get_response(
    URI.parse("http://www.websequencediagrams.com/index.php#{out['img']}"))
  att_data = ""
  response.body.each_byte {|byte| att_data << byte }
  encoded_data = Base64.encode64(att_data)

  # Upload the image to Confluence using the addAttachment JSON-RPC api
  response = upload_attachment_to_confluence(out['img'].split('=')[1]+".png",params['pageId'],"image/png",
                                             encoded_data,params['user_id'])
  # Parse response then generate snippet to send back to Confluence
  out = MultiJson.decode(response.body)
  "<ac:image ac:title=\"#{out['title']}\"><ri:attachment ri:filename=\"#{out['fileName']}\"/></ac:image>"
end

get "/dump" do
  snoop
end

private

def upload_attachment_to_confluence(file_name, page_id, content_type, data, user_id)
  # Generate post body (JSON) using the Light Protocol described here:
  # https://developer.atlassian.com/display/CONFDEV/Confluence+JSON-RPC+APIs#ConfluenceJSON-RPCAPIs-TheLightProtocol
  payload = MultiJson.encode([page_id,{
    :fileName => file_name, 
    :pageId => page_id,
    :contentType => content_type
  }, data])

  signed_http.post( "#{ENV['APP_CONTEXT_PATH']}/rpc/json-rpc/confluenceservice-v2/addAttachment?user_id=#{user_id}",
                     payload, {"Content-Type" => "application/json"} )
end

def snoop
  headers = env.inject({}){|acc, (k,v)| acc[$1.downcase] = v if k =~ /^http_(.*)/i; acc}
  out = "<h4>Headers</h4><pre>#{CGI.escapeHTML(headers.inspect.gsub('",',"\",\n"))}</pre>"
  out += "<h4>Method</h4><pre>#{CGI.escapeHTML(request.request_method)}</pre>"
  out += "<h4>URL</h4><pre>#{CGI.escapeHTML(request.url)}</pre>"
  out += "<h4>ENV</h4><pre>#{CGI.escapeHTML(request.env.inspect.gsub('",',"\",\n"))}</pre>"
end

def signed_http
  consumer = OAuth::Consumer.new(
    ENV["APP_KEY"],
    OpenSSL::PKey::RSA.new(ENV["LOCAL_PRIVATE_KEY"]),
    :site => ENV["APP_BASE_URL"],
    :signature_method => 'RSA-SHA1',
    :request_token_path => "",
    :authorize_path => "",
    :access_token_path => "")
  consumer.http.set_debug_output($stderr)
  OAuth::AccessToken.new(consumer)
end
