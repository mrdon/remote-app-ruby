# Atlassian Remote App Examples

This is a collection of Remote App examples for Atlassian OnDemand. The app is 
currently [deployed on Heroku](http://glowing-fog-6327.heroku.com/register) and
running on <https://remoteapps.jira.com>.

For more info about Atlassian's Remote Apps project, visit 
<https://remoteapps.jira.com/wiki/display/ARA/Tech+Spec>.
